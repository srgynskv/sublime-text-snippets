# sublime-text-snippets

Custom snippets for Sublime Text.

Save files to the snippets directory (e.g., Linux `~/.config/sublime-text-3/Packages/User`), and relaunch Sublime Text.
